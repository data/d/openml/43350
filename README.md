# OpenML dataset: US-Breweries

https://www.openml.org/d/43350

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
While brainstorming ideas for a statistics project for a course last semester, the idea of utilizing data about microbreweries came up. Unfortunately after some exploration and thought, we scrapped the idea, but I've got this data to show for it anyway.

Content
Data was acquired from https://www.beermonthclub.com/ and contains information about breweries available on the site as of October 1st, 2019. Name, type of brewery, address, website, and state are among the data included.
Code used to scrape is available at https://github.com/brkurzawa/brewpub-scraper

Acknowledgements
Thanks to beermonthclub for allowing the /brewpub section to be scraped.

Inspiration
This data alone isn't too inspiring, but maybe it could be used as the basis for an analysis project that looks at information such as yelp reviews, or number of links to a brewery's website.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43350) of an [OpenML dataset](https://www.openml.org/d/43350). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43350/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43350/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43350/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

